// Copyright (c) 2011 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

var test_time_ms = 60 * 60 * 1000;
var should_scroll = true;
var should_scroll_up = true;
var scroll_loop = false;
var scroll_interval_ms = 1000;
var scroll_by_pixels = 600;
var tasks = "";
