# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import logging

from autotest_lib.client.bin import utils
from autotest_lib.client.common_lib import error
from autotest_lib.client.cros import touch_playback_test_base


class touch_UpdateErrors(touch_playback_test_base.touch_playback_test_base):
    """Check that touch update is tried and that there are no update errors."""
    version = 1

    def _check_updates(self):
        """Fail the test if device has problems with touch firmware update.

        @raises: TestFail if no update attempt occurs or if there is an error.

        """
        log_cmd = 'grep -i touch /var/log/messages'

        pass_terms = ['chromeos-touch-firmware-update']
        fail_terms = ['error', 'fail']

        # Check for key terms in touch logs.
        for term in pass_terms + fail_terms:
            search_cmd = '%s | grep -i %s' % (log_cmd, term)
            log_entries = utils.run(search_cmd, ignore_status=True).stdout
            if term in fail_terms and len(log_entries) > 0:
                raise error.TestFail('Error: %s.' % log_entries.split('\n')[0])
            if term in pass_terms and len(log_entries) == 0:
                raise error.TestFail('Touch firmware did not attempt update.')

    def run_once(self):
        """Entry point of this test."""

        # Skip run on devices which do not have touch inputs.
        if not self._has_touchpad and not self._has_touchscreen:
            logging.info('This device does not have a touch input source.')
            return

        self._check_updates()
