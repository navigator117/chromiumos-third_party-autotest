# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""FAFT config setting overrides for Alex."""


class Values(object):
    """FAFT config values for Alex."""
    keyboard_dev = False
    gbb_version = 1.0
    has_eventlog = False
