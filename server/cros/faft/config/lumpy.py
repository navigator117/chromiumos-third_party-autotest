# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""FAFT config setting overrides for Lumpy."""


class Values(object):
    """FAFT config values for Lumpy."""
    keyboard_dev = False
    has_eventlog = False
