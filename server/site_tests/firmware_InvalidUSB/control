# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from autotest_lib.server import utils

AUTHOR = "Chrome OS Team"
NAME = "firmware_InvalidUSB"
PURPOSE = "Servo based booting an invalid USB image test"
CRITERIA = "This test will fail if the invalid USB boots successfully"
SUITE = "faft,faft_bios,faft_normal,faft_lv4"
TIME = "SHORT"
TEST_CATEGORY = "Functional"
TEST_CLASS = "firmware"
TEST_TYPE = "server"

DOC = """
This test requires a USB disk plugged-in, which contains a Chrome OS test
image (built by "build_image --test"). On runtime, this test corrupts the
USB image and tries to boot into it. A failure is expected. It then
restores the USB image and boots into it again.
"""

args_dict = utils.args_to_dict(args)
servo_args = hosts.CrosHost.get_servo_arguments(args_dict)

def run_invalidusb(machine):
    host = hosts.create_host(machine, servo_args=servo_args)
    job.run_test("firmware_InvalidUSB", host=host, cmdline_args=args,
                 disable_sysinfo=True, dev_mode=False, tag="normal")

parallel_simple(run_invalidusb, machines)
