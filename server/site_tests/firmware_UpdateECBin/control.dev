# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from autotest_lib.server import utils

AUTHOR = "Chrome OS Team"
NAME = "firmware_UpdateECBin"
PURPOSE = "Servo based software sync EC update test"
CRITERIA = "This test will fail if software sync failed on EC update"
SUITE = "faft_update_ec"
TIME = "SHORT"
TEST_CATEGORY = "Functional"
TEST_CLASS = "firmware"
TEST_TYPE = "server"

DOC = """
This test verified the EC software sync. The EC binary is embedded in
the BIOS image. On AP startup, AP verifies the EC by comparing the
hash of the current EC with the hash of the embedded EC image. So
updating EC is simple and we just need to change the EC binary on
the BIOS image.

The test steps are:
 1. check EC is RO boot and AP is RO boot,
    extract the original EC binary on current AP RW A and save it locally,
    copy the new_ec binary to the EC area on the current AP RW A,
    disable RO_BOOT preamble flag,
    reboot;
 2. check EC is RW boot and AP is RW A boot,
    compare the current EC with the new_ec binary,
    copy the original EC binary to the EC area on the current AP RW A,
    reboot;
 3. check EC is RW boot and AP is RW A boot,
    compare the current EC with the original EC binary,
    enable RO_BOOT preamble flag,
    reboot;
 4. check EC is RO boot and AP is RW A boot.

This test requires a new EC image for update. The new EC image should
be different with the existing EC image on the current BIOS. In normal
cases, we use the ec_autest_image.bin from firmware_from_source.tar.bz2.
The ec_autest_image.bin is the same binary as ec.bin but shift'ed and its
version string has the "_shift" suffix.

The new EC image should be specified by the new_ec argument, like:
  run_remote_tests.sh --args "new_ec=ec_autest_image.bin" ...

So RONORMAL->TWOSTOP, TWOSTOP->TWOSTOP, and TWOSTOP->RONORMAL are covered.
"""

args_dict = utils.args_to_dict(args)
servo_args = hosts.CrosHost.get_servo_arguments(args_dict)

def run_updateec(machine):
    host = hosts.create_host(machine, servo_args=servo_args)
    job.run_test("firmware_UpdateECBin", host=host, cmdline_args=args,
                 disable_sysinfo=True, dev_mode=True, tag="dev")

parallel_simple(run_updateec, machines)
