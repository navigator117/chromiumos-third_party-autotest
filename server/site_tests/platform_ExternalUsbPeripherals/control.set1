# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from autotest_lib.server import utils

AUTHOR = "Chrome OS Team"
NAME = "ExternalUsbPeripheralsSet1"
PURPOSE = "Servo based USB boot stress test"
CRITERIA = "This test will fail if any of the actions or checks fail."
TIME = "LONG"
TEST_CATEGORY = "Functional"
TEST_CLASS = "platform"
TEST_TYPE = "server"
SUITE = "kernel_usb_set1"
DEPENDENCIES = "servo"

DOC = """
This test uses servo to connect/disconnect servo USB hub before and
after events like reboot, login, suspend, resume etc.

The test fails if
- crash files are generated
- device is pingable when suspended
- wrong action passed through action_sequence flag
- USB detected peripherals are different than expected
- there is no servo board attached
- USB peripherals checks(usb_checks below) on command line fail
Other detection checks can be added for each peripheral

Set1 is set of four USB peripherals plugged
- USB headset
- USB HD Webcam - should be Logitech HD Pro Webcam C920
- USB stick with four partitions named ExFAT  Ext4  FAT  NTFS
"""

args_dict = utils.args_to_dict(args)
servo_args = hosts.CrosHost.get_servo_arguments(args_dict)

def run(machine):
    host = hosts.create_host(machine, servo_args=servo_args)

    repeat = int(args_dict.get("repeat", 2))
    crash_check = args_dict.get("crash_check", "true").upper() == "TRUE"

    default_actions = str("reboot,unplug,plug,login,unplug,plug,"
                          "suspend,resume,unplug,suspend,plug,resume,"
                          "suspend,unplug,resume,plug")

    action_sequence = str(args_dict.get("action_sequence", default_actions))

    usb_list = ["C-Media Electronics, Inc. Audio Adapter",
                "Logitech, Inc. HD Pro Webcam C920",
                "Kingston Technology Company Inc.",
               ]
    usb_checks = {
        # USB Audio Output devices
        str("cras_test_client --dump_server_info | "
            "awk '/Output Devices:/,/Output Nodes:/'") :
            ["USB Headphone Set: USB Audio" ],
        # USB Audio Input devices
        str("loggedin:cras_test_client --dump_server_info | "
            "awk '/Input Devices:/,/Input Nodes:/'") :
            ["USB Headphone Set: USB Audio", "HD Pro Webcam C920: USB Audio"],
        # USB stick four partitions volumes
        "loggedin:ls -l /media/removable/" :
            ["ExFAT", "Ext4", "FAT", "NTFS"],
        # USB Web camera
        "cat /sys/class/video4linux/video*/name" :
            ["HD Pro Webcam C920"],
        }

    job.run_test("platform_ExternalUsbPeripherals", host=host,
                 disable_sysinfo=True, client_autotest="desktopui_SimpleLogin",
                 action_sequence=action_sequence, repeat=repeat,
                 usb_list = usb_list, usb_checks=usb_checks,
                 crash_check=crash_check)

parallel_simple(run, machines)
