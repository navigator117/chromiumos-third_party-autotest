# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

AUTHOR = "gwendal"
NAME = "StorageQualRetention.quick"
SUITE = "storage_qual_quick, kernel_weekly_regression"
TIME = "LONG"
TEST_CATEGORY = "Stress"
TEST_CLASS = "suite"
TEST_TYPE = "server"

DOC = """
Sequence for testing the Storage Qual running from the fixed devices.
This tests check retentions.
"""
HOUR_IN_SECS=60*60
DAY_IN_SECS=24*HOUR_IN_SECS

# tag is specified twice, because it is popped in _build_tagged_test_name().
SERVER_TESTS= [
    ('hardware_StorageQualBase', {
        'tag': 'before', 'client_tag': 'before', 'crypto_runtime': 30}),
    ('hardware_StorageStress', {'duration': HOUR_IN_SECS, 'tag': 'soak',
        'power_command': 'nothing', 'storage_test_command': 'full_write'}),
    ('hardware_StorageStress', {'duration': HOUR_IN_SECS, 'tag': 'suspend',
        'power_command': 'suspend', 'suspend_duration': 120}),
    ('hardware_StorageQualBase', {
        'tag': 'after', 'client_tag': 'after', 'crypto_runtime': 30}),
]

def run_server_tests(machine):
    job.add_sysinfo_logfile('/var/log/storage_info.txt', on_every_test=True)
    for test, argv in SERVER_TESTS:
        job.run_test(test, client_ip=machine, **argv)

parallel_simple(run_server_tests, machines)
