# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

AUTHOR = "Chrome OS Team"
NAME = "cellular_mbim_compliance"
TIME = "SHORT"
TEST_CATEGORY = "Functional"
TEST_CLASS = "suite"
TEST_TYPE = "server"

DOC = """
Run the MBIM compliance test suite on a given modem device
@param build: The name of the image to test.
              Ex: x86-mario-release/R17-1412.33.0-a1-b29
@param board: The board to test on.  Ex: x86-mario
@param pool: The pool of machines to utilize for scheduling. If pool=None
             board is used.
@param check_hosts: require appropriate live hosts to exist in the lab.
"""

import common
from autotest_lib.server.cros import provision
from autotest_lib.server.cros.dynamic_suite import dynamic_suite

# Values specified in this bug template will override default values when
# filing bugs on tests that are a part of this suite. If left unspecified
# the bug filer will fallback to it's defaults.
_BUG_TEMPLATE = {
    'labels': ['Cr-OS-Systems-Mobile'],
    'owner': '',
    'status': None,
    'summary': None,
    'title': None,
    'cc': ['benchan@chromium.org', 'pprabhu@chromium.org',
           'rpius@chromium.org', 'thieule@chromium.org']
}

dynamic_suite.reimage_and_run(
    build=build, board=board, name='cellular_mbim_compliance', job=job, pool=pool,
    check_hosts=check_hosts, add_experimental=True, num=num,
    file_bugs=file_bugs, max_runtime_mins=20, priority=priority,
    timeout_mins=timeout_mins, bug_template=_BUG_TEMPLATE,
    devserver_url=devserver_url, suite_dependencies='modem_repair',
    version_prefix=provision.CROS_VERSION_PREFIX,
    wait_for_results=wait_for_results, job_retry=job_retry,
    max_retries=max_retries)
