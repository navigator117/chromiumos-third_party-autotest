# Copyright (c) 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

AUTHOR = "chromeos-chameleon"
NAME = "chameleon_hdmi_weekly"
PURPOSE = "A Chameleon test weekly suite."
CRITERIA = "All tests with SUITE=chameleon_hdmi_weekly must pass."

TIME = "LENGTHY"
TEST_CATEGORY = "General"
TEST_CLASS = "suite"
TEST_TYPE = "Server"

DOC = """
Display tests which require Chameleon board connected via HDMI connection.
The Chameleon board can emulate a monitor such that the test can control
its behaviors in order to test the Chrome OS graphic stack.

@param build: The name of the image to test.
              Ex: x86-mario-release/R17-1412.33.0-a1-b29
@param board: The board to test on.  Ex: x86-mario
@param pool: The pool of machines to utilize for scheduling. If pool=None
             board is used.
@param check_hosts: require appropriate live hosts to exist in the lab.
@param SKIP_IMAGE: (optional) If present and True, don't re-image devices.
"""

import common
from autotest_lib.server.cros import provision
from autotest_lib.server.cros.dynamic_suite import dynamic_suite

dynamic_suite.reimage_and_run(
    build=build, board=board, name=NAME, job=job, pool=pool,
    check_hosts=check_hosts, add_experimental=True, num=num,
    file_bugs=file_bugs, priority=priority,
    suite_dependencies='chameleon:hdmi', timeout_mins=timeout_mins,
    max_runtime_mins=240, devserver_url=devserver_url,
    version_prefix=provision.CROS_VERSION_PREFIX,
    wait_for_results=wait_for_results, job_retry=job_retry,
    max_retries=max_retries)
