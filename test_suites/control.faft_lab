# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

AUTHOR = "mohsinally"
NAME = "FAFT Lab Suite"
TIME = "MEDIUM"
TEST_CATEGORY = "General"
TEST_CLASS = "suite"
TEST_TYPE = "Server"

DOC = """
FAFT in the lab suite.

@param build: The name of the image to test.
              Ex: x86-mario-release/R17-1412.33.0-a1-b29
@param board: The board to test on. Ex: x86-mario
@param pool: The pool of machines to utilize for scheduling. If pool=None
             board is used.
@param check_hosts: require appropriate live hosts to exist in the lab.
@param SKIP_IMAGE: (optional) If present and True, don't re-image devices.
"""

import common
from autotest_lib.server.cros import provision
from autotest_lib.server.cros.dynamic_suite import dynamic_suite


# Values specified in this bug template will override default values when
# filing bugs on tests that are a part of this suite. If left unspecified
# the bug filer will fallback to it's defaults.
_BUG_TEMPLATE = {
    'labels': ['FW-labblocker', 'Restrict-View-Google'],
    'owner': '',
    'status': None,
    'summary': None,
    'title': None,
    'cc': ['mohsinally@google.com',
           'beeps@chromium.org']
}

dynamic_suite.reimage_and_run(
    build=build, board=board, name='faft_lab', job=job, pool=pool,
    check_hosts=check_hosts, add_experimental=True, num=num,
    file_bugs=True, priority=priority, timeout_mins=timeout_mins,
    bug_template=_BUG_TEMPLATE, devserver_url=devserver_url,
    version_prefix=provision.CROS_VERSION_PREFIX,
    wait_for_results=wait_for_results, job_retry=job_retry,
    max_retries=max_retries)
