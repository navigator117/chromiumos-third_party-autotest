# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

AUTHOR = "Chrome OS Team"
NAME = "stress3"
PURPOSE = "Stress test functionality and devices."
CRITERIA = "All tests with SUITE=stress3 must pass."

TIME = "LONG"
TEST_CATEGORY = "Stress"
TEST_CLASS = "suite"
TEST_TYPE = "Server"

DOC = """
This is the stress3 test suite.

All tests should pass for devices to be considered shippable. This suite is
meant to cover the full gamut of stress long running stress tests. Other stress
related suites will focus on more specific aspects, which can be run as
diagnosis tools.

@param build: The name of the image to test.
              Ex: x86-mario-release/R17-1412.33.0-a1-b29
@param board: The board to test on.  Ex: x86-mario
@param pool: The pool of machines to utilize for scheduling. If pool=None
             board is used.
@param check_hosts: require appropriate live hosts to exist in the lab.
@param SKIP_IMAGE: (optional) If present and True, don't re-image devices.
"""

import common
from autotest_lib.server.cros import provision
from autotest_lib.server.cros.dynamic_suite import dynamic_suite

_BUG_TEMPLATE = {
    'labels': ['OS-Chrome', 'StressRack-Bug'],
    'owner': 'mohsinally@chromium.org',
}


dynamic_suite.reimage_and_run(
    build=build, board=board, name='stress3', job=job, pool=pool,
    check_hosts=check_hosts, add_experimental=True, num=num,
    file_bugs=file_bugs, priority=priority, timeout_mins=timeout_mins,
    devserver_url=devserver_url, version_prefix=provision.CROS_VERSION_PREFIX,
    wait_for_results=wait_for_results, job_retry=job_retry,
    max_retries=max_retries, bug_template=_BUG_TEMPLATE)
