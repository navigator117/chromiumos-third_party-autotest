# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

AUTHOR = "Chrome OS Team,chromeos-video@google.com"
NAME = "video_weekly_regression"
PURPOSE = "Test required video functionality - takes > 1hour/platform"
CRITERIA = "All tests with SUITE=video_weekly_regression must pass."

TIME = "LONG"
TEST_CATEGORY = "General"
TEST_CLASS = "suite"
TEST_TYPE = "Server"

DOC = """
This is the Video Weekly Regression Test suite.  Failures in this test
suite should not close the tree. It should consist of tests that are too long
to run in the related daily or per-build regression test suites.

@param build: The name of the image to test.
              Ex: x86-mario-release/R17-1412.33.0-a1-b29
@param board: The board to test on.  Ex: x86-mario
@param pool: The pool of machines to utilize for scheduling. If pool=None
             board is used.
@param check_hosts: require appropriate live hosts to exist in the lab.
@param SKIP_IMAGE: (optional) If present and True, don't re-image devices.
"""

import common
from autotest_lib.server.cros import provision
from autotest_lib.server.cros.dynamic_suite import dynamic_suite

dynamic_suite.reimage_and_run(
    build=build, board=board, name='video_weekly_regression', job=job,
    pool=pool, check_hosts=check_hosts, add_experimental=True, num=num,
    file_bugs=file_bugs, priority=priority, timeout_mins=timeout_mins,
    max_runtime_mins=240, devserver_url=devserver_url,
    version_prefix=provision.CROS_VERSION_PREFIX,
    wait_for_results=wait_for_results, job_retry=job_retry,
    max_retries=max_retries)
